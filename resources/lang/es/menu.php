<?php

return [

    'home'                  => 'Inicio',
    'request'               =>'Solicitudes',
    'checkin'               => 'Checkins',
    'actividades'           => 'Actividades Hotel',
    'listaactividad'        => 'Lista Actividades',
    'itinerario'            => 'Itinerario',
    'comidabebidas'         => 'Alimentos y Bebidas',
    'platillos'             => 'Platillos',
    'vinos'                 => 'Vinos',
    'ventas'                => 'Ventas',
    'menu'                  => 'Menu',
    'locaciones'            => 'Locaciones',
    'restaurante'           =>'Restaurants',
    'spa'                   => 'SPA',
    'servicios'             => 'Servicios',
    'cenas'                 => 'Momentos Especiales',
    'servicioshabitacion'   => 'Servicios a habitación',
    'habitaciones'          => 'Habitaciones',
    'goout'                 => 'Go out',
    'hoteles'               => 'Hoteles',
    'listahotel'            => 'Lista Hoteles',
    'listalobby'            => 'Lista Lobbys',
    'usuarios'              => 'Usuarios',
    'Usuarios VIP'          =>'VIP Users',
    'Nuevo usuario VIP'     => 'New VIP User',
    'promociones'           => 'Promociones',
    'promociones disponibles'   => 'Promociones disponibles',
    'nuevas promociones'    => 'New Promotions',
    'promovalida'           => 'Pendientes de aprobación',
    'karisma'               => 'Karisma Experience',

    'locations'             => 'Locaciones',

    'hotel_activities'      => 'Actividades Hotel',
    'activities_list'       => 'Lista de Actividades',
    'activities_itinerary'  => 'Itinerario',
    'promotions'            => 'Promociones',
    'promotions_list'       => 'Lista de Promociones',
    'promotions_approvals'  => 'Aprobación de Promociones',

    'food_wine'             => 'Alimentos y Bebidas',
    'wine_list'             => 'Lista de Vinos',
    'wine_sales'            => 'Venta Vinos',
    'dish_list'             => 'Platillos',
    'dish_types_list'       => 'Tipos de Platillos',

    'guest_services'        => 'Servicio al Huesped',
    'car_rental'            => 'Renta de Autos',

    'starbucks_menu'        => 'Menu Starbucks',
    'starbucks_types'       => 'Tipos de Producto',
    'starbucks_extras'      => 'Ingredientes Extra',


    'idioma'    => 'Idioma',
    'cuenta'    => 'Mi cuenta',
    'perfil'    => 'Perfil',
    'salir'     => 'Salir',

];
