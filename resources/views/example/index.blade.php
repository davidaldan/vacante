@extends('master.master')

@section('title','xxx')

@section('css')
    {!! Html::style('css/jquery-ui.css') !!}
    {!! Html::style('css/bootstrap-formhelpers.css') !!}
    {!! Html::style('css/alertify.min.css') !!}
    {!! Html::style('css/alertify-default-theme.min.css') !!}
    {!! Html::style('css/loading_gif.css') !!}
@endsection

@section('include')

@endsection

@section('content')
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="page-content">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar content-box" style="display: block;">
                    @include('include.menu')
                </div>
            </div>
            <div class="col-md-10">

                <div class="row">
                    <div class="col-md-12">
                        <nav class="breadcrumb">
                            <a class="breadcrumb-item" href="{{ URL::to('/') }}">xxx</a>
                            <span class="breadcrumb-item active">xxx</span>
                        </nav>
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="col-md-6"><a class="new-item" style="cursor:pointer">Nuevo</a></div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>xxx</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody-refresh">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

@section('scripts')
    {!! Html::script('js/jquery-ui.js') !!}
    {!! Html::script('js/bootstrap-waitingfor.js') !!}
    {!! Html::script('js/bootstrap-formhelpers.js') !!}
    {!! Html::script('js/bootstrap-validator.min.js') !!}
    {!! Html::script('js/alertify.min.js') !!}
    {!! Html::script('js/bootstrap-notify.min.js') !!}
@endsection