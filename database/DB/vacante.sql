-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-12-2019 a las 17:16:53
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vacante`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hoteles`
--

CREATE TABLE `hoteles` (
  `id` int(11) NOT NULL,
  `googleid` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `telephon` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `lat` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL,
  `lng` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'Logo del hotel',
  `img` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hoteles`
--

INSERT INTO `hoteles` (`id`, `googleid`, `name`, `text`, `address`, `telephon`, `lat`, `lng`, `icon`, `img`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ChIJG5YGwKSHTo8RADnxmsLcNvI', 'Azul Beach Resort Riviera Maya', 'The experience at Azul Beach Resort Riviera Maya, by Karisma begins the moment you arrive, with a champagne toast check in for adults, Karisma’s signature aromatherapy and pillow menu to choose from and Nickelodeon smoothies for kids. With only 148 deluxe rooms and suites, this beachfront oasis caters to families and couples who desire a refined yet more intimate experience. Everything is just a short stroll or stroller ride away, from the half-mile white-sand beach to the five restaurants, swim-up bars, Azulitos by Nickelodeon Kids Club and Vassa Spa.', 'direccion XXX', '01111111', '20.9045415', '-86.8476315', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/azul_beach.jpg', 'AzulBeachResortRivieraMayac4ca4238a0b923820dcc509a6f75849b', 1, NULL, '2017-05-03 19:52:20'),
(2, 'ChIJL7KKFCyVb4YRJW_WDXpHkAE', 'Azul Beach Resort The Fives Playa del Carmen', 'Whoever said youth is wasted on the young has never experienced Azul Beach Resort The Fives Playa del Carmen, by Karisma. Because for tots, teens, couples, groups, honeymooners and even brides-to-be, this Gourmet Inclusive® playground will bring out the playful side in everyone. At the same time, Azul Beach Resort The Fives Playa del Carmen’ luxury accommodations and impeccable service will satisfy the most discriminating adult tastes.', 'direccion XXX', '100010101', '20.664892', '-87.034587', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/the-fives-azul.jpg', 'AzulBeachResortTheFivesPlayadelCarmenc81e728d9d4c2f636f067f89cc14862c', 1, NULL, '2017-05-03 19:52:20'),
(6, 'ChIJ4XqmCTaGTo8R5nO3nFlzZYU', 'Azul Beach Resort Sensatori Mexico', 'With 435 Jacuzzi® Suites, gourmet cuisine and world-class Vassa Spa, Azul Beach Resort Sensatori Mexico offers the Karisma Gourmet Inclusive® Experience on a grand scale.\nYet as expansive as this multi-generational Caribbean resort is, every guest is treated like royalty. You see, the Azul Sensatori staff has mastered the art of attending to and even anticipating your every wish – even when your greatest desire is to be left alone.', 'direccion XXX', '3213232', '20.897237', '-86.856276', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/sensa-mexico.jpg', 'AzulBeachResortSensatoriMexico', 1, NULL, NULL),
(7, 'ChIJaymvEJkN2Y4RcKFz1fzq1S0', 'Azul Beach Resort Sensatori Jamaica', 'Situated on a coveted stretch of Negril’s world-famous Seven Mile beach, Azul Beach Resort Sensatori Jamaica is a pristine paradise where couples, families, friends and colleagues can reconnect beneath brilliant blue skies and tropical sunshine.', 'direccion XXX', '3213213', '18.330746', '-78.336454', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/azul-sensa-jamaica.jpg', 'sensatori-punta-canac81e728d9d4c2f636f067f89cc14862c', 1, NULL, NULL),
(8, 'ChIJj4hYeixiTo8RLqXWRmYnlQ0', 'El Dorado Royale Resort', 'El Dorado Royale, by Karisma, is an adults-only paradise where everything is designed to help you slow down, relax, and savor every moment of your vacation.\nYou’ll find world-class restaurants, renowned chefs, personal concierges and bars that pour from the top shelf. We harvest vegetables from our own 76,000 square-foot hydroponic greenhouse. We offer fitness classes, dance lessons and wine tastings. Twice a month, we host a culinary series featuring visiting chefs from around the world.', 'Carretera Cancún - Tulum: KM 45\r\nRiviera Maya, Quintana Roo\r\nPlaya del Carmen, QROO,\r\nMéxico C.P. 77710', '351351', '20.786974', '-86.939357', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/dorado-royale.jpg', 'DoradoRoyaleResort', 1, NULL, NULL),
(9, 'ChIJGXUTeixiTo8RC1VIHfnkPtY', 'El Dorado Casitas Royale', 'El Dorado Casitas Royale, by Karisma is a private enclave dedicated to providing an even higher level of luxury and personal attention. You’re free to enjoy all of the dazzling amenities, restaurants and activities of neighboring El Dorado Royale and Generations Riviera Maya, but guests of Casitas Royale gain exclusive access to their own private pools and swim-up bars, a host of concierge services, aromatherapy and pillow menus and so much more.', 'Carretera Cancún - Tulum Km. 45, 77710 Tulum, QROO, México', '011.52 (998) 872-8030', '20.786974', '-86.939357', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/dorado-casitas.jpg', 'ElDoradoCasitasRoyale', 1, NULL, NULL),
(10, 'ChIJLa_P4ChDTo8RiCAz9YEVsgI', 'El Dorado Maroma', 'Travel Channel named Maroma Beach one of the 10 best beaches in the world. El Dorado Maroma sits right in the heart of it. The beach will seduce you. But it’s the entire experience at El Dorado Maroma that will make you fall in love.', 'Carretera Cancún – Tulum, Km 55.3\r\nPlaya del Carmen, Quintana Roo\r\nMéxico C.P. 77710', '011.52 (984) 206-3477', '20.723342', '-86.978286', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/dorado-maroma.jpg', 'ElDoradoMaromaHighlights', 1, NULL, NULL),
(11, 'ChIJJZdoCRhDTo8RTzB3S8D916Y', 'El Dorado Seaside Suites', 'Two distinct experiences in beachside luxury, all within one award-winning resort – this is the AAA Four Diamond El Dorado Seaside Suites. This stunning, adults-only paradise located on Kantenah Bay is divided into two distinct sections, Palms and Infinity, each offering a unique experience in Mexican hospitality.', 'Carr. Cancún - Tulum 95, Puerto Aventuras, Q.R.', '01 984 875 1910', '20.4544906', '-87.2719886', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/dorado-seaside.jpg', 'ElDoradoSeasideSuitesHighlights', 1, NULL, NULL),
(12, 'ChIJ2egAEEj6ShMRS55J7TKYTrM', 'Sensimar Makarska', 'This season, get spoiled in unspoiled nature. SENSIMAR Makarska is a gorgeous, intimate hotel catering to adults who wish to unwind in a dazzling, natural setting. The carefully remodeled hotel was specially designed in order to ensure a peaceful vibe. Enthusiastic and attentive staff, hushed colors, tasteful décor and fragrant pine forests will instantly help you find your happy place in Igrane. Our single aim is to make your vacation in SENSIMAR Makarska an exceptional experience. We guarantee you will feel right at home no matter if you choose a striking sea view room or a sensuous swim up suite. Take a dip to cool down in one of the outdoor pools just to warm up for the exquisite food served in our contemporary restaurants fit for connoisseurs. Also, there is a FKK beach located just 5 minutes walking distance from hotel.', 'IGRANE 320, 21329, Igrane, Croatia', '021 604 222', '43.194386', '17.137391', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/Sensimar-Makarska.jpg', 'TUI-Sensimar-Makarska', 1, NULL, NULL),
(13, 'ChIJY6tE0sX7ShMRrmn05X18lFU', 'Sensimar Adriatic Beach Resort', 'Don’t just have a holiday. Have a love affair. The moment you set foot in Croatia the passionate, dedicated team of our beachfront hotel will ensure you are every bit relaxed as you are entertained. SENSIMAR Adriatic Beach Resort is a place where understated elegance combines with luxurious amenities and services. Newly remodeled contemporary rooms offer stunning sea views of the crystal clear Adriatic.', 'Croatia, D8 87, 21329, Živogošće, Croatia', NULL, '43.1846487', '17.1629911', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/Sensimar-Adriatic-Beach.jpg', 'SENSIMAR-Adriatic-Beach-Resort', 1, NULL, NULL),
(14, 'ChIJhSm4VN2KSxMRTJ4GKBqpP7Y', 'Sensimar Kalamota Island Resort', 'At SENSIMAR Kalamota Island Resort guests are courted with nature, assisted by surroundings that provide a full experience of a well-deserved holiday. The property and professional staff are intertwined to stimulate all senses, pastimes and leisure pursuits. The resort is located on a beach that extends to the little village and port, offering seclusion and ample space for the guests that wish to retain their peace. SENSIMAR Kalamota Island Resort is perfect for adults who choose to be pampered in nature. Active relaxation in intimate surroundings is what awaits the guests.', 'Donje Celo bb, 20221, Koločep, Croatia', '020 312 150', '42.676857', '18.004191', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/SENSIMAR-Kalamota.jpg', 'SENSIMAR-Kalamota-Island-Resort', 1, NULL, NULL),
(15, 'ChIJd4_RDQ0rTI8RT1EOi_DW_rU', 'Generations Riviera Maya', 'Welcome to the quintessential family resort – the All Suite, All Butler, All Gourmet Generations Riviera Maya. Here, you will discover everything from Gourmet Inclusive® à la carte cuisine to infinity pool balcony suites and attentive butler service.', 'Carretera Cancún Tulum Km. 45, Rivera Maya, 77710 Cancun, Q.R., Mexico', '+52 998 872 8030', '20.7937035', '-86.9317348', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/generations-rivieramaya.jpg', 'Generations-Riviera-Maya', 1, NULL, NULL),
(16, 'ChIJN2SoLKvDqI4RDJ18ZN26ojs', 'Azul Beach Resort Sensatori Punta Cana', 'A blissful retreat is made up of many pieces. A serene setting, delectable meals, reliable service, and exciting activities. But what if there’s a place that took all of those pieces, and added a sense of unmistakable distinctiveness? Settings wouldn’t just be serene, they would be indescribably breathtaking. Meals wouldn’t just be delectable, they would be an adventure in mastered culinary arts. Service wouldn’t just be reliable, it would be impeccable. And activities wouldn’t just be exciting, they would be downright thrilling. Perfection lies in the details, and there’s no detail left unnoticed at Sensatori Resort Punta Cana.', 'Punta Cana 23000, Dominican Republic', '(809) 833-4560', '18.8014396', '-68.5724537', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/azul-seansatori-puntacana.jpg', 'Sensatori-Punta-Cana', 1, NULL, NULL),
(17, 'ChIJb2LqCHUv9o4RofQ3ESTy4_s', 'Allure Chocolat', 'Located in the heart of Cartagena, Colombia historic old town surrounded by the colonial city wall. Our classical façade hides an interior of a noble and sophisticated architecture featuring modern accommodations. The waterfront hotel boasts a rooftop infinity pool with truly spectacular city views.', 'Calle del Arsenal, Calle 24 # 8B-58, Cartagena', '+1 866-527-4762', '10.4202381', '-75.5482792', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/allure-chocolat.jpg', 'Allure-Chocolat', 1, NULL, NULL),
(18, 'ChIJBXHDprd6WkcRNBOxTGMMg1s', 'Allure Caramel', 'Allure Caramel Hotel by Karisma, a boutique art gallery hotel displays the original and reproduction pieces of art that marked the art scene through the first decades of 20th Century in the region, taking you through the history of both the region, and Europe. Located in Dorcol, the oldest part of Belgrade, in a beautiful villa built in 1920\'s. Allure Caramel Hotel is a unique combination of elegance, art and comfort that will make your stay unforgettable. Walking through its premises we travel back in time and imagine extravagant galas and feasts held here which were considered one of the main gathering places for the crème of the Belgrade social circles at that time.', '11000, Venizelosova 31, Beograd, Serbia', '011 3349572', '44.819754', '20.474119', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/allure-caramel.jpg', 'Allure-Caramel', 1, NULL, NULL),
(19, 'ChIJNwAma5Ml9o4RBcbPhnB2N18', 'Allure Bonbon', 'Feed your sense of wanderlust and escape from the mundane to boutique Allure Hotels, by Karisma, a part of our award-winning luxury hotel collection. These uniquely envisioned creations suit the busy lifestyle of the adventurous, modern traveler seeking ultimate extravagance. In true Karisma Hotels & Resorts fashion, Allure spaces in Colombia and Serbia are utterly sophisticated, dedicated to flawless service and of impeccable standard', 'Calle del Arsenal, Calle 24 # 10-32, Cartagena, Getsemaní, Colombia', '(866) 527-4762', '10.418785', '-75.54687', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/allure-bonbon.jpg', 'Allure-Bonbon', 1, NULL, NULL),
(20, 'ChIJBeR02iRDTo8R5A3M-SD2mVQ', 'Hidden Beach Resort', 'Luxurious, yet simple. Distinguished, yet welcoming. Welcome to Hidden Beach, the only 5 Star, adults-only nudist resort in Mexico. With 43 beachfront suites only one hour and a half south of the Cancun Airport, Hidden Beach is a gem waiting to be discovered. We’ve designed the most sumptuous Gourmet Inclusive® resort with a blend of unmatched signature services, allowing you to release your stresses, and relax in blissful confidence.', 'Carretera Cancún - Tulum Km 95, Kantenah, 77710 Solidaridad, Q.R., Mexico', '01 984 875 1910', '20.4539735', '-87.2726962', NULL, 'http://www.apiazulrivieramaya.muchomil.com/azulrivieramaya/public/images/resorts/Hidden-Beach-Resort.jpg', 'Hidden+Beach+Resort', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hoteles_idioma`
--

CREATE TABLE `hoteles_idioma` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `idioma` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hoteles_idioma`
--

INSERT INTO `hoteles_idioma` (`id`, `name`, `text`, `hotel_id`, `idioma`, `created_at`, `updated_at`) VALUES
(1, 'Azul Beach Resort Riviera Maya', 'La experiencia en Azul Beach Resort Riviera Maya, \r\nby Karisma comienza desde el momento en que usted llega,\r\ncon un champán de bienvenida para los adultos durante el check-in, aromaterapia exclusiva de Karisma y opciones de almohadas, además de licuados de Nickelodeon para los niños.\r\nEste oasis frente a la playa, que solo cuenta con 148\r\nhabitaciones de lujo y suites, alberga a familias y parejas\r\nque desean una experiencia refinada y más íntima.  \r\nTodo está a solo un corto paseo o en carritos, desde la playa de arena blanca de media milla hasta los cinco restaurantes, los bares en la piscina, Azulitos by Nickelodeon Kids Club y Vassa Spa.', 1, 'es', NULL, NULL),
(2, 'Azul Beach Resort Riviera Maya', 'The experience at Azul Beach Resort Riviera Maya, by Karisma begins the moment you arrive, with a champagne toast check in for adults, Karisma’s signature aromatherapy and pillow menu to choose from and Nickelodeon smoothies for kids. With only 148 deluxe rooms and suites, this beachfront oasis caters to families and couples who desire a refined yet more intimate experience. Everything is just a short stroll or stroller ride away, from the half-mile white-sand beach to the five restaurants, swim-up bars, Azulitos by Nickelodeon Kids Club and Vassa Spa.', 1, 'en', NULL, NULL),
(3, 'Azul Beach Resort The Fives Playa del Carmen', 'Quien dijo la juventud se desperdicia en los jovenes nunca conoció Azul Beach Resort The Fives Playa del Carmen, by Karisma. Porque esta área recreativa Gourmet Inclusive® sacará el lado divertido de todos: niños, adolescentes, parejas, grupos, recien casados y hasta futuras novias.\r\nAl mismo tiempo, las habitaciones de lujo y el servicio impecable de Azul Beach Resort The Fives Playa del Carmen satisfarán hasta los gustos de los adultos más exigentes.\r\n', 2, 'es', NULL, NULL),
(4, 'Azul Beach Resort The Fives Playa del Carmen', 'Whoever said youth is wasted on the young has never experienced Azul Beach Resort The Fives Playa del Carmen, by Karisma. Because for tots, teens, couples, groups, honeymooners and even brides-to-be, this Gourmet Inclusive® playground will bring out the playful side in everyone. At the same time, Azul Beach Resort The Fives Playa del Carmen’ luxury accommodations and impeccable service will satisfy the most discriminating adult tastes.', 2, 'en', NULL, NULL),
(5, 'Azul Beach Resort Sensatori Mexico', 'Azul Sensatori tiene 435 Jacuzzi® Suites, cocina gourmet y el Vassa Spa de primera clase y ofrece la experiencia Karisma Gourmet Inclusive® a gran escala.\r\nSi bien este resort caribeno multigeneracional es grande, cada huésped recibe un tratamiento de realeza. El personal de Azul Sensatori es experto en el arte de la atención y hasta anticipa cada uno de sus deseos, aún cuando su mayor deseo sea estar solo.', 6, 'es', NULL, NULL),
(6, 'Azul Beach Resort Sensatori Mexico', 'With 435 Jacuzzi® Suites, gourmet cuisine and world-class Vassa Spa, Azul Beach Resort Sensatori Mexico offers the Karisma Gourmet Inclusive® Experience on a grand scale.\nYet as expansive as this multi-generational Caribbean resort is, every guest is treated like royalty. You see, the Azul Sensatori staff has mastered the art of attending to and even anticipating your every wish – even when your greatest desire is to be left alone.', 6, 'en', NULL, NULL),
(7, 'Azul Beach Resort Sensatori Jamaica', 'Azul Sensatori Jamaica, ubicado en una franja codiciada de la Playa de Siete Millas mundialmente famosa de Negril, es un paraíso prístino donde parejas, familias, amigos y colegas pueden reconectarse bajo el cielo azul brillante y el sol tropical.', 7, 'es', NULL, NULL),
(8, 'Azul Beach Resort Sensatori Jamaica', 'Situated on a coveted stretch of Negril’s world-famous Seven Mile beach, Azul Beach Resort Sensatori Jamaica is a pristine paradise where couples, families, friends and colleagues can reconnect beneath brilliant blue skies and tropical sunshine.', 7, 'en', NULL, NULL),
(9, 'El Dorado Royale Resort', 'El Dorado Royale, by Karisma, es un paraíso para adultos donde todo está diseñado para ayudarlo a tranquilzarse, relajarse y disfrutar de cada momento de sus vaciones.\r\nEncontrara restaurantes de primer nivel, reconocidos chefs, concierges personales y bares que sirvan marcas de primera calidad. Cosechamos vegetales de nuestro propio invernadero hidropónico de 76,000 pies cuadrados.\r\nOfrecemos clases de acondicionamiento físico, lecciones de baile y degustacion de vinos.\r\nDos veces al mes, organizamos una serie culinaria con chefs visitantes de todo el mundo.', 8, 'es', NULL, NULL),
(10, 'El Dorado Royale Resort', 'El Dorado Royale, by Karisma, is an adults-only paradise where everything is designed to help you slow down, relax, and savor every moment of your vacation.\nYou’ll find world-class restaurants, renowned chefs, personal concierges and bars that pour from the top shelf. We harvest vegetables from our own 76,000 square-foot hydroponic greenhouse. We offer fitness classes, dance lessons and wine tastings. Twice a month, we host a culinary series featuring visiting chefs from around the world.', 8, 'en', NULL, NULL),
(11, 'El Dorado Casitas Royale', 'El Dorado Casitas Royale by Karisma es un espacio privado dedicado a ofrecer un mayor nivel de lujo y atención personal. Tiene la libertad de disfrutar de todos los servicios, los restaurantes y las actividades espectaculares de nuestros vecinos El Dorado Royale y Generations Riviera Maya, pero los huespedes de Casitas Royale además tienen acceso exclusivo a sus propias piscinas privadas y bares Swim-Up, una gama de servicios de concierge, opciones de almohadas y aromaterapia y muchísimo más.', 9, 'es', NULL, NULL),
(12, 'El Dorado Casitas Royale', 'El Dorado Casitas Royale, by Karisma is a private enclave dedicated to providing an even higher level of luxury and personal attention. You’re free to enjoy all of the dazzling amenities, restaurants and activities of neighboring El Dorado Royale and Generations Riviera Maya, but guests of Casitas Royale gain exclusive access to their own private pools and swim-up bars, a host of concierge services, aromatherapy and pillow menus and so much more.', 9, 'en', NULL, NULL),
(13, 'El Dorado Maroma', 'Travel Channel reconoció a playa Maroma como una de las mejores 10 playas del mundo. El Dorado Maroma se encuentra justo en el centro. La playa lo seducirá. Pero la experiencia completa de El Dorado Maroma es lo que hará que se enamore.', 10, 'es', NULL, NULL),
(14, 'El Dorado Maroma', 'Travel Channel named Maroma Beach one of the 10 best beaches in the world. El Dorado Maroma sits right in the heart of it. The beach will seduce you. But it’s the entire experience at El Dorado Maroma that will make you fall in love.', 10, 'en', NULL, NULL),
(15, 'El Dorado Seaside Suites', 'Dos experiencias bien singulares en el lujo junto a la playa, todo dentro del galardonado resort, estas son las  suites de El Dorado Seaside Suites con el premio AAA Four Diamond. Este increíble paraíso solo para adultos en Kantenah Bay está dividido en dos partes diferentes, Palms y Infinity, donde cada una ofrece una experiencia única de la industria hotelera mexicana.', 11, 'es', NULL, NULL),
(16, 'El Dorado Seaside Suites', 'Two distinct experiences in beachside luxury, all within one award-winning resort – this is the AAA Four Diamond El Dorado Seaside Suites. This stunning, adults-only paradise located on Kantenah Bay is divided into two distinct sections, Palms and Infinity, each offering a unique experience in Mexican hospitality.', 11, 'en', NULL, NULL),
(17, 'Sensimar Makarska', 'Esta temporada, déjate mimar por la naturaleza virgen. SENSIMAR Makarska es un magnífico e íntimo hotel para adultos que desean relajarse en un entorno deslumbrante y natural. El hotel, cuidadosamente remodelado, fue especialmente diseñado para garantizar un ambiente tranquilo. Personal entusiasta y atento, colores silenciosos, decoración de buen gusto y fragantes bosques de pinos le ayudarán instantáneamente a encontrar su lugar feliz en Igrane. Nuestro único objetivo es hacer de sus vacaciones en SENSIMAR Makarska una experiencia excepcional. Le garantizamos que se sentirá como en casa sin importar si elige una habitación con vista al mar o una suite sensual. Tómese un chapuzón para refrescarse en una de las piscinas al aire libre para calentarse y disfrutar de la exquisita comida que se sirve en nuestros restaurantes contemporáneos aptos para conocedores. Además, hay una playa FKK situada a sólo 5 minutos a pie del hotel.', 12, 'es', NULL, NULL),
(18, 'Sensimar Makarska', 'This season, get spoiled in unspoiled nature. SENSIMAR Makarska is a gorgeous, intimate hotel catering to adults who wish to unwind in a dazzling, natural setting. The carefully remodeled hotel was specially designed in order to ensure a peaceful vibe. Enthusiastic and attentive staff, hushed colors, tasteful décor and fragrant pine forests will instantly help you find your happy place in Igrane. Our single aim is to make your vacation in SENSIMAR Makarska an exceptional experience. We guarantee you will feel right at home no matter if you choose a striking sea view room or a sensuous swim up suite. Take a dip to cool down in one of the outdoor pools just to warm up for the exquisite food served in our contemporary restaurants fit for connoisseurs. Also, there is a FKK beach located just 5 minutes walking distance from hotel.', 12, 'en', NULL, NULL),
(19, 'Sensimar Adriatic Beach Resort', 'No solo unas vacaciones. Tenga una historia de amor. En el momento en que ponga un pie en Croacia, el apasionado y dedicado equipo de nuestro hotel frente a la playa le asegurará que estará lo más relajado posible mientras esté entretenido. SENSIMAR Adriatic Beach Resort es un lugar donde la elegancia discreta se combina con lujosas comodidades y servicios. Las habitaciones contemporáneas recién remodeladas ofrecen impresionantes vistas al mar Adriático.', 13, 'es', NULL, NULL),
(20, 'Sensimar Adriatic Beach Resort', 'Don’t just have a holiday. Have a love affair. The moment you set foot in Croatia the passionate, dedicated team of our beachfront hotel will ensure you are every bit relaxed as you are entertained. SENSIMAR Adriatic Beach Resort is a place where understated elegance combines with luxurious amenities and services. Newly remodeled contemporary rooms offer stunning sea views of the crystal clear Adriatic.', 13, 'en', NULL, NULL),
(21, 'Sensimar Kalamota Island Resort', 'En SENSIMAR Kalamota Island Resort los huéspedes son cortejados por la naturaleza, asistidos por un entorno que les proporciona una experiencia completa de unas merecidas vacaciones. La propiedad y el personal profesional están entrelazados para estimular todos los sentidos, pasatiempos y actividades de ocio. El complejo está situado en una playa que se extiende hasta el pequeño pueblo y el puerto, ofreciendo aislamiento y amplio espacio para los huéspedes que deseen mantener su paz. SENSIMAR Kalamota Island Resort es perfecto para adultos que eligen ser mimados en la naturaleza. La relajación activa en un entorno íntimo es lo que espera a los huéspedes.', 14, 'es', NULL, NULL),
(22, 'Sensimar Kalamota Island Resort', 'At SENSIMAR Kalamota Island Resort guests are courted with nature, assisted by surroundings that provide a full experience of a well-deserved holiday. The property and professional staff are intertwined to stimulate all senses, pastimes and leisure pursuits. The resort is located on a beach that extends to the little village and port, offering seclusion and ample space for the guests that wish to retain their peace. SENSIMAR Kalamota Island Resort is perfect for adults who choose to be pampered in nature. Active relaxation in intimate surroundings is what awaits the guests.', 14, 'en', NULL, NULL),
(23, 'Generations Riviera Maya', 'Bienvenido a Generations Riviera Maya, el gran resort familiar para disfrutar de las mejores suites con servicio de mayordomo y una excepcional experiencia gourmet. Aquí, descubrirá desde cocina a la carta Gourmet Inclusive® hasta suites con balcón y piscina infinity y un cordial servicio de mayordomo.', 15, 'es', NULL, NULL),
(24, 'Generations Riviera Maya', 'Welcome to the quintessential family resort – the All Suite, All Butler, All Gourmet Generations Riviera Maya. Here, you will discover everything from Gourmet Inclusive® à la carte cuisine to infinity pool balcony suites and attentive butler service.', 15, 'en', NULL, NULL),
(25, 'Azul Beach Resort Sensatori Punta Cana', 'Un retiro maravilloso se compone de muchas piezas. Un ambiente sereno, comidas deliciosas, servicio confiable y actividades emocionantes. ¿Pero qué tal si hay un lugar que tomó todas esas piezas, y agregó un sentido de inconfundible distinción? Los ajustes no sólo serían serenos, sino que serían indescriptiblemente impresionantes. Las comidas no sólo serían deliciosas, sino que serían una aventura en las artes culinarias dominadas. El servicio no sólo sería confiable, sino impecable. Y las actividades no sólo serían emocionantes, sino francamente emocionantes. La perfección está en los detalles, y no hay detalle que pase desapercibido en Sensatori Resort Punta Cana.', 16, 'es', NULL, NULL),
(26, 'Azul Beach Resort Sensatori Punta Cana', 'A blissful retreat is made up of many pieces. A serene setting, delectable meals, reliable service, and exciting activities. But what if there’s a place that took all of those pieces, and added a sense of unmistakable distinctiveness? Settings wouldn’t just be serene, they would be indescribably breathtaking. Meals wouldn’t just be delectable, they would be an adventure in mastered culinary arts. Service wouldn’t just be reliable, it would be impeccable. And activities wouldn’t just be exciting, they would be downright thrilling. Perfection lies in the details, and there’s no detail left unnoticed at Sensatori Resort Punta Cana.', 16, 'en', NULL, NULL),
(27, 'Allure Chocolat', 'Está ubicado en el corazón de Cartagena, el histórico casco antiguo de Colombia rodeado por el muro de la ciudad colonial. Nuestra fachada clásica oculta un interior de arquitectura noble y sofisticada que cuenta con habitaciones modernas. Este hotel frente al mar tiene una piscina infinity en la terraza con vistas a la ciudad realmente espectaculares.', 17, 'es', NULL, NULL),
(28, 'Allure Chocolat', 'Located in the heart of Cartagena, Colombia historic old town surrounded by the colonial city wall. Our classical façade hides an interior of a noble and sophisticated architecture featuring modern accommodations. The waterfront hotel boasts a rooftop infinity pool with truly spectacular city views.', 17, 'en', NULL, NULL),
(29, 'Allure Caramel', 'Allure Caramel Hotel by Karisma, un hotel boutique con galería de arte, exhibe obras de arte originales y réplicas que dejaron su impronta en la escena artística durante las primeras décadas del siglo XX en la región, para ayudarle a recorrer la historia del lugar y de Europa. Ubicado en Dorcol, el sector más antiguo de Belgrado, en un hermoso chalet construido en la década de 1920. Allure Caramel Hotel es una combinación única de elegancia, arte y comodidad que hará que su estadía sea inolvidable. Al caminar por sus instalaciones viajamos en el tiempo e imaginamos las extravagantes galas y festines que tuvieron lugar allí, uno de los principales lugares de reunión para los círculos sociales más altos de Belgrado en ese momento.', 18, 'es', NULL, NULL),
(30, 'Allure Caramel', 'Allure Caramel Hotel by Karisma, a boutique art gallery hotel displays the original and reproduction pieces of art that marked the art scene through the first decades of 20th Century in the region, taking you through the history of both the region, and Europe. Located in Dorcol, the oldest part of Belgrade, in a beautiful villa built in 1920\'s. Allure Caramel Hotel is a unique combination of elegance, art and comfort that will make your stay unforgettable. Walking through its premises we travel back in time and imagine extravagant galas and feasts held here which were considered one of the main gathering places for the crème of the Belgrade social circles at that time.', 18, 'en', NULL, NULL),
(31, 'Allure Bonbon', 'Estremezca sus sentidos y escápese de lo cotidiano con Allure Hoteles Boutique, by Karisma, parte de nuestra galardonada colección de hoteles de lujo. Esta exclusiva colección ha sido creada para adaptarse al estilo de vida dinámico del viajero moderno en busca de lo excepcional. Fiel al estilo de Karisma Hotels & Resorts, el diseño de Allure en Colombia y Serbia es absolutamente sofisticado, el servicio excepcional y los estándares óptimos', 19, 'es', NULL, NULL),
(32, 'Allure Bonbon', 'Feed your sense of wanderlust and escape from the mundane to boutique Allure Hotels, by Karisma, a part of our award-winning luxury hotel collection. These uniquely envisioned creations suit the busy lifestyle of the adventurous, modern traveler seeking ultimate extravagance. In true Karisma Hotels & Resorts fashion, Allure spaces in Colombia and Serbia are utterly sophisticated, dedicated to flawless service and of impeccable standard', 19, 'en', NULL, NULL),
(33, 'Hidden Beach Resort', 'Lujoso, pero simple. Distinguido, pero acogedor. Bienvenido a Hidden Beach, el único resort nudista 5 estrellas para adultos en México. Con 43 suites frente a la playa a solo una hora y media al sur del aeropuerto de Cancún, Hidden Beach es una joya a la espera de ser descubierta. Hemos diseñado el resort Gourmet Inclusive® más suntuoso con una combinación de servicios unicos, que le permite liberar su estrés y relajarse en una feliz confianza.', 20, 'es', NULL, NULL),
(34, 'Hidden Beach Resort', 'Luxurious, yet simple. Distinguished, yet welcoming. Welcome to Hidden Beach, the only 5 Star, adults-only nudist resort in Mexico. With 43 beachfront suites only one hour and a half south of the Cancun Airport, Hidden Beach is a gem waiting to be discovered. We’ve designed the most sumptuous Gourmet Inclusive® resort with a blend of unmatched signature services, allowing you to release your stresses, and relax in blissful confidence.', 20, 'en', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_habitacion`
--

CREATE TABLE `tipo_habitacion` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `desc1` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `desc2` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `price` double(24,4) DEFAULT NULL,
  `adults1` int(11) DEFAULT NULL,
  `adults2` int(11) DEFAULT NULL,
  `ninos` int(11) DEFAULT NULL,
  `pies` double(24,2) DEFAULT NULL,
  `meters` double(24,2) DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_habitacion`
--

INSERT INTO `tipo_habitacion` (`id`, `name`, `desc1`, `desc2`, `price`, `adults1`, `adults2`, `ninos`, `pies`, `meters`, `img`, `hotel_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'DELUXE ROOM', 'These spacious rooms with garden or ocean front feature a king-size or two double beds for adults.', 'These spacious rooms with garden or ocean front feature a king-size or two double beds for adults.', NULL, 3, 2, 2, 344.00, 32.00, 'habitaciones/c4ca4238a0b923820dcc509a6f75849b/deluxeRoomGallery_1.jpg', 1, 1, NULL, NULL),
(2, 'JACUZZI JUNIOR SUITE', 'These large, recently renovated suites boast two queen beds and contemporary Caribbean furnishings.', 'These large, recently renovated suites boast two queen beds and contemporary Caribbean furnishings.', NULL, 3, 2, 2, 608.00, 57.00, 'habitaciones/c81e728d9d4c2f636f067f89cc14862c/jacuzziJuniorSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(3, 'JACUZZI SWIM-UP JUNIOR SUITE', 'Large, recently renovated suite.', 'Large, recently renovated suite with 2 queen-sized beds, designed in a contemporary Caribbean style with dark wood decoration and direct pool access from the terrace.', NULL, 3, 2, 2, 608.00, 57.00, 'habitaciones/eccbc87e4b5ce2fe28308fd9f2a7baf3/royalSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(4, 'FAMILY SUITE', 'versized suite specially designed for families.', 'Oversized suite specially designed for families with a private king-size bed and separate living area with a sofa bed designed to sleep 3 children under the age of 12.', NULL, 2, 2, 3, 856.00, 80.00, 'habitaciones/a87ff679a2f3e71d9181a67b7542122c/familySuiteGallery_1.jpg', 1, 1, NULL, NULL),
(5, 'FAMILY SWIM-UP SUITE', 'These expansive family friendly suites feature a private king-sized', 'These expansive family friendly suites feature a private king-sized bedroom area and a separate living room with a sofa bed designed to accommodate 3 children under the age of 12. The suite also boasts direct pool access from the terrace.', NULL, 2, 2, 3, 856.00, 80.00, 'habitaciones/e4da3b7fbbce2345d7772b0674a318d5/jacuzziOceanfrontSwimUpJrSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(6, 'HONEYMOON SUITE', 'Large, recently renovated suites designed with contemporary Caribbean.', 'Large, recently renovated suites designed with contemporary Caribbean furnishings feature a king bed and a separate living area with a sofa bed.', NULL, 2, 2, 0, 783.00, 73.00, 'habitaciones/1679091c5a880faf6fb5e6087eb1b2dc/royalSwimUpSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(7, 'WEDDING SUITE', 'Designed with brides in mind, the Wedding Suite includes a king-sized bed.', 'Designed with brides in mind, the Wedding Suite includes a king-sized bed, a private vanity area with makeup station, lounge chair, sitting area and even a mannequin for the bride’s dress. And to help brides unwind before the big day, there’s an in-suite Jacuzzi®.', NULL, 2, 2, 0, 732.00, 68.00, 'habitaciones/8f14e45fceea167a5a36dedd4bea2543/honeymoonWeddingSuite_1.jpg', 1, 1, NULL, NULL),
(8, 'AZUL SUITE', 'Una suite de gran tamaño diseñada con estilo caribeño', 'Una suite de gran tamaño diseñada con estilo caribeño, una cama tamaño King con dosel, un Jacuzzi® para dos dentro de la suite, una amplia cama de playa, un área de estar y una terraza.', NULL, 2, 2, 0, 667.00, 62.00, 'habitaciones/c9f0f895fb98ab9159f51fd0297e236d/Azul_Suite_1.jpg', 1, 0, NULL, NULL),
(9, 'ROYAL SUITE', 'Large, recently renovated suite designed in a contemporary Caribbean style.', 'Large, recently renovated suite designed in a contemporary Caribbean style with dark wood decoration, 1 king bed or 2 queen beds, modern living area and luxury bathroom.', NULL, 2, 2, 2, 626.00, 58.00, 'habitaciones/45c48cce2e2d7fbdea1afc51c7c6ad26/azulSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(10, 'ROYAL SWIM-UP SUITE', 'Large, recently renovated suite designed in a contemporary Caribbean style.', 'Large, recently renovated suite designed in a contemporary Caribbean style with dark wood decoration, 2 queen beds or 1 king bed, modern living area, luxury bathroom area, equipped with flat screen TV and direct pool access from the terrace.', NULL, 2, 2, 2, 626.00, 58.00, 'habitaciones/d3d9446802a44259755d38e6d163e820/azulSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(11, 'CONNOISSEUR SUITE', 'These spacious 783-square foot suites take luxury to a new level. ', 'These spacious 783-square foot suites take luxury to a new level. Chic Caribbean styling creates an elegant haven, with a spacious living room designed for relaxation or entertaining, offering breathtaking ocean views from the private terrace. Utterly sophisticated and completely indulgent, the separate bedroom becomes a soothing sanctuary complete with a marble bathroom lavish with every amenity. Your personal Majordomo is on hand to schedule private in-suite dinners and provide service that is attentive yet unobtrusive.', NULL, 2, 2, 2, 783.00, 73.00, 'habitaciones/6512bd43d9caa6e02c990b0a82652dca/azulSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(12, 'CONNOISSEUR SWIM-UP SUITE', 'Discover the allure of water with a 783-square foot suite.', 'Discover the allure of water with a 783-square foot suite where you can step from your patio directly into a dreamy river pool that flows up to your doorstep. Chic Caribbean styling indoors creates an elegant haven. A spacious living room has been designed for relaxation or entertaining, while the separate bedroom becomes a soothing sanctuary complete with a marble bathroom. Your personal Majordomo is on hand to schedule private in-suite dinners and provide service that is attentive yet unobtrusive.', NULL, 2, 2, 2, 783.00, 73.00, 'habitaciones/c20ad4d76fe97759aa27a0c99bff6710/azulSuiteGallery_1.jpg', 1, 1, NULL, NULL),
(13, 'VIP', NULL, NULL, NULL, 0, 0, 0, 0.00, 0.00, NULL, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_habitacion_idiomas`
--

CREATE TABLE `tipo_habitacion_idiomas` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `desc1` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `desc2` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `tipo_habitacion_id` int(11) DEFAULT NULL,
  `idioma` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_habitacion_idiomas`
--

INSERT INTO `tipo_habitacion_idiomas` (`id`, `name`, `desc1`, `desc2`, `tipo_habitacion_id`, `idioma`, `created_at`, `updated_at`) VALUES
(1, 'HABITACIÓN DELUXE', 'Estas amplias habitaciones​​​​​​​ frente al mar o al jardín poseen una cama tamaño King o dos camas dobles para adultos.', 'Estas amplias habitaciones​​​​​​​ frente al mar o al jardín poseen una cama tamaño King o dos camas dobles para adultos.', 1, 'es', NULL, NULL),
(2, 'DELUXE ROOM', 'These spacious rooms with garden or ocean front feature a king-size or two double beds for adults.', 'These spacious rooms with garden or ocean front feature a king-size or two double beds for adults.', 1, 'en', NULL, NULL),
(3, 'JACUZZI JUNIOR SUITE', 'Estas amplias y flamantes suites poseen dos camas tamaño Queen y muebles contemporáneos de estilo caribeño.', 'Estas amplias y flamantes suites poseen dos camas tamaño Queen y muebles contemporáneos de estilo caribeño.', 2, 'es', NULL, NULL),
(4, 'JACUZZI JUNIOR SUITE', 'These large, recently renovated suites boast two queen beds and contemporary Caribbean furnishings.', 'These large, recently renovated suites boast two queen beds and contemporary Caribbean furnishings.', 2, 'en', NULL, NULL),
(5, 'JACUZZI SWIM-UP JUNIOR SUITE', 'Una amplia y flamante suite', 'Una amplia y flamante suite con 2 camas tamaño Queen, diseñada con un estilo caribeño contemporáneo y detalles decorativos en madera oscura y acceso directo a la piscina desde la terraza.', 3, 'es', NULL, NULL),
(6, 'JACUZZI SWIM-UP JUNIOR SUITE', 'Large, recently renovated suite.', 'Large, recently renovated suite with 2 queen-sized beds, designed in a contemporary Caribbean style with dark wood decoration and direct pool access from the terrace.', 3, 'en', NULL, NULL),
(7, 'SUITE FAMILIAR', 'Una suite de gran tamaño diseñada especialmente para familias.', 'Una suite de gran tamaño diseñada especialmente para familias que incluye un sector privado con cama tamaño King y una sala de estar independiente con un sofá cama con capacidad para 3 niños menores de 12 años.', 4, 'es', NULL, NULL),
(8, 'FAMILY SUITE', 'versized suite specially designed for families.', 'Oversized suite specially designed for families with a private king-size bed and separate living area with a sofa bed designed to sleep 3 children under the age of 12.', 4, 'en', NULL, NULL),
(9, 'SUITE FAMILIAR SWIM-UP', 'Estas espaciosas suites para familias cuentan con un dormitorio privado con cama tamaño King', 'Estas espaciosas suites para familias cuentan con un dormitorio privado con cama tamaño King y una sala independiente con un sofá cama con capacidad para 3 niños menores de 12 años. La suite también posee acceso directo a la piscina desde la terraza.', 5, 'es', NULL, NULL),
(10, 'FAMILY SWIM-UP SUITE', 'These expansive family friendly suites feature a private king-sized', 'These expansive family friendly suites feature a private king-sized bedroom area and a separate living room with a sofa bed designed to accommodate 3 children under the age of 12. The suite also boasts direct pool access from the terrace.', 5, 'en', NULL, NULL),
(11, 'SUITE LUNA DE MIEL', 'Amplias y flamantes suites diseñadas con muebles contemporáneos de estilo caribeño.', 'Amplias y flamantes suites diseñadas con muebles contemporáneos de estilo caribeño con una cama tamaño King y una sala de estar independiente con un sofá cama.', 6, 'es', NULL, NULL),
(12, 'HONEYMOON SUITE', 'Large, recently renovated suites designed with contemporary Caribbean.', 'Large, recently renovated suites designed with contemporary Caribbean furnishings feature a king bed and a separate living area with a sofa bed.', 6, 'en', NULL, NULL),
(13, 'SUITE MATRIMONIAL', 'Diseñada pensando en las novias.', 'Diseñada pensando en las novias, la Suite Matrimonial incluye una cama tamaño King, un área privada con tocador y sector de maquillaje, una tumbona, un área de estar e incluso un maniquí para colgar el vestido de novia. Y para asistir a las novias antes del gran día, hay un Jacuzzi® para dos dentro de la suite.', 7, 'es', NULL, NULL),
(14, 'WEDDING SUITE', 'Designed with brides in mind, the Wedding Suite includes a king-sized bed.', 'Designed with brides in mind, the Wedding Suite includes a king-sized bed, a private vanity area with makeup station, lounge chair, sitting area and even a mannequin for the bride’s dress. And to help brides unwind before the big day, there’s an in-suite ', 7, 'en', NULL, NULL),
(15, 'AZUL SUITE', 'Una suite de gran tamaño diseñada con estilo caribeño', 'Una suite de gran tamaño diseñada con estilo caribeño, una cama tamaño King con dosel, un Jacuzzi® para dos dentro de la suite, una amplia cama de playa, un área de estar y una terraza.', 8, 'es', NULL, NULL),
(16, 'AZUL SUITE', 'Una suite de gran tamaño diseñada con estilo caribeño', 'Una suite de gran tamaño diseñada con estilo caribeño, una cama tamaño King con dosel, un Jacuzzi® para dos dentro de la suite, una amplia cama de playa, un área de estar y una terraza.', 8, 'en', NULL, NULL),
(17, 'ROYAL SUITE', 'Una amplia y flamante suite diseñada con un estilo caribeño contemporáneo', 'Una amplia y flamante suite diseñada con un estilo caribeño contemporáneo y detalles decorativos en madera oscura, con 1 cama tamaño King o 2 camas tamaño Queen, una sala de estar moderna y un baño lujoso.', 9, 'es', NULL, NULL),
(18, 'ROYAL SUITE', 'Large, recently renovated suite designed in a contemporary Caribbean style.', 'Large, recently renovated suite designed in a contemporary Caribbean style with dark wood decoration, 1 king bed or 2 queen beds, modern living area and luxury bathroom.', 9, 'en', NULL, NULL),
(19, 'ROYAL SWIM-UP SUITE', 'Una amplia y flamante suite diseñada con un estilo caribeño contemporáneo', 'Una amplia y flamante suite diseñada con un estilo caribeño contemporáneo y detalles decorativos en madera oscura, con 2 camas tamaño Queen o 1 cama tamaño King, una sala de estar moderna, un baño lujoso con televisor de pantalla plana y acceso directo a la piscina desde la terraza.', 10, 'es', NULL, NULL),
(20, 'ROYAL SWIM-UP SUITE', 'Large, recently renovated suite designed in a contemporary Caribbean style.', 'Large, recently renovated suite designed in a contemporary Caribbean style with dark wood decoration, 2 queen beds or 1 king bed, modern living area, luxury bathroom area, equipped with flat screen TV and direct pool access from the terrace.', 10, 'en', NULL, NULL),
(21, 'CONNOISSEUR SUITE', 'Estas amplias suites de 783 ft² llevan la extravagancia a otro nivel.', 'Estas amplias suites de 783 ft² llevan la extravagancia a otro nivel. El elegante diseño caribeño crea un refugio de ensueño, con una sala amplia diseñada para relajarse o divertirse, y ofrece imponentes vistas al mar desde la terraza privada. Totalmente sofisticado y acogedor, el dormitorio separado se convierte en un santuario relajante con un baño de mármol lujoso que ofrece todos los servicios. Su mayordomo personal estará disponible para programar cenas en la privacidad de su suite y ofrecerle sus servicios de manera cordial pero no invasiva.', 11, 'es', NULL, NULL),
(22, 'CONNOISSEUR SUITE', 'These spacious 783-square foot suites take luxury to a new level. ', 'These spacious 783-square foot suites take luxury to a new level. Chic Caribbean styling creates an elegant haven, with a spacious living room designed for relaxation or entertaining, offering breathtaking ocean views from the private terrace. Utterly sophisticated and completely indulgent, the separate bedroom becomes a soothing sanctuary complete with a marble bathroom lavish with every amenity. Your personal Majordomo is on hand to schedule private in-suite dinners and provide service that is attentive yet unobtrusive.', 11, 'en', NULL, NULL),
(23, 'CONNOISSEUR SWIM-UP SUITE', 'Descubra el encanto del agua con una suite de 783 ft².', 'Descubra el encanto del agua con una suite de 783 ft² que le permite salir de su patio directamente a una piscina de río lento soñada que llega justo hasta su puerta. El elegante diseño caribeño en el interior la convierte en un elegante refugio. La amplia sala ha sido diseñada para relajarse o divertirse, mientras que el dormitorio separado se convierte en un santuario relajante con un baño de mármol. Su mayordomo personal estará disponible para programar cenas en la privacidad de su suite y ofrecerle sus servicios de manera cordial pero no invasiva.', 12, 'es', NULL, NULL),
(24, 'CONNOISSEUR SWIM-UP SUITE', 'Discover the allure of water with a 783-square foot suite.', 'Discover the allure of water with a 783-square foot suite where you can step from your patio directly into a dreamy river pool that flows up to your doorstep. Chic Caribbean styling indoors creates an elegant haven. A spacious living room has been designed for relaxation or entertaining, while the separate bedroom becomes a soothing sanctuary complete with a marble bathroom. Your personal Majordomo is on hand to schedule private in-suite dinners and provide service that is attentive yet unobtrusive.', 12, 'en', NULL, NULL),
(25, 'VIP', NULL, NULL, 13, 'es', NULL, NULL),
(26, 'VIP', NULL, NULL, 13, 'en', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `hoteles`
--
ALTER TABLE `hoteles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hoteles_idioma`
--
ALTER TABLE `hoteles_idioma`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_habitacion`
--
ALTER TABLE `tipo_habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_habitacion_idiomas`
--
ALTER TABLE `tipo_habitacion_idiomas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `hoteles`
--
ALTER TABLE `hoteles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `hoteles_idioma`
--
ALTER TABLE `hoteles_idioma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `tipo_habitacion`
--
ALTER TABLE `tipo_habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tipo_habitacion_idiomas`
--
ALTER TABLE `tipo_habitacion_idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
